Feature: Merge Lead for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads
And Enter the Username
And Enter the Password
When Click on Login Button
Then Verify User navigates to Home Page
When Click on CRMSFA Link
Then Verify User navigates to Leads page
When Click on Leads Link
Then Verify User navigates to My Leads page

Scenario Outline: TC006 Merge Lead
When Click on Merge Leads option
Then Verify User navigates to Merge Leads page

When Click on From Lead Icon
When Switch to Find Leads page
Then Verify User navigates to Find Leads page
Given Enter Lead Id <fromLeadId>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
When Click on Resulting Lead Id
Then Switch to Merge Leads page 

When Click on To Lead Icon
When Switch to Find Leads page again
Then Verify User navigates to Find Leads page
Given Enter Lead Id <toLeadId>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
When Click on Resulting Lead Id
Then Switch to Merge Leads page 

When User clicks on Merge button
And Switch to Alert
And Accept the Alert
Then Verify User navigates to View Lead page

When Click on Find Leads option
Then Verify User navigates to Find Leads page
Given Enter Lead Id <fromLeadId>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
Then Verify Error Message <errorMessage>

Examples:
|fromLeadId|toLeadId|errorMessage|
|12100|12104|No records to display|