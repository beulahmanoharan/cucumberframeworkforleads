Feature: Edit Lead for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads
And Enter the Username
And Enter the Password
When Click on Login Button
Then Verify User navigates to Home Page
When Click on CRMSFA Link
Then Verify User navigates to Leads page
When Click on Leads Link
Then Verify User navigates to My Leads page

Scenario Outline: TC003 Edit Lead
When Click on Find Leads option
Then Verify User navigates to Find Leads page
Given Enter the First Name as search criteria <firstName>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
When Click on First Resulting Lead
Then Verify User navigates to View Lead page
When Click on Edit button
Then Verify Fields are editable
Given Update Company Name <companyName>
When Click Update Lead button
Then Verify user navigates to View Lead page
Given Updated Company Name is fetched
Then Verify the updated company name matches

Examples:
|firstName|companyName|
|Beulah|SG|