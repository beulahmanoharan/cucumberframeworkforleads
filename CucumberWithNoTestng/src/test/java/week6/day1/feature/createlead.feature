Feature: Create Lead for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads
And Enter the Username
And Enter the Password
When Click on Login Button
Then Verify User navigates to Home Page
When Click on CRMSFA Link
Then Verify User navigates to Leads page
When Click on Leads Link
Then Verify User navigates to My Leads page

Scenario Outline: TC002 Create Lead
When Click on Create Leads option
Then Verify User navigates to Create Lead page 
Given Enter First Name <firstname>
Given Enter Last Name <lastname>
Given Enter Company Name <companyname>
When Click on Create Leads button
Then New Lead has been Created
Given First Name is fetched
Then Verify First Name matches

Examples:
|firstname|lastname|companyname|
|Beulah|Anderson|SG|
|Santhiya|T|SG|

