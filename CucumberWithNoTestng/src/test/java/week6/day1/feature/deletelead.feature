Feature: Delete Lead for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads
And Enter the Username
And Enter the Password
When Click on Login Button
Then Verify User navigates to Home Page
When Click on CRMSFA Link
Then Verify User navigates to Leads page
When Click on Leads Link
Then Verify User navigates to My Leads page

Scenario Outline: TC004 Delete Lead
When Click on Find Leads option
Then Verify User navigates to Find Leads page
When Click on Phone tab
Then User navigates to the Page with Phone Number search criteria
Given Enter Phone Number as search criteria <countryCode> <areaCode> <phoneNumber>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
When Click on First Resulting Lead Id
Then Verify User navigates to View Lead page
When Click on Delete button
Then Verify User navigates to My Leads page
When Click on Find Leads option
Then Verify User navigates to Find Leads page
Given Enter Lead Id
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
Then Verify Error Message <errorMessage>


Examples:
|countryCode|areaCode|phoneNumber|errorMessage|
|91|044|6777899|No records to display|