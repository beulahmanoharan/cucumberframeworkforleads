package week6.day1.runCucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/week6/day1/feature/mergelead.feature",glue="week6/day1/steps", 
monochrome=true
/*,dryRun=true, snippets=SnippetType.CAMELCASE*/)
public class RunCucumberMergeLeadTest extends AbstractTestNGCucumberTests{

}
