package week6.day1.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginCucumber extends LeadTest
{
	
	@Given("Open the Chrome Browser")
	public void openTheChromeBrowser() {
	    // Write code here that turns the phrase above into concrete actions
	  
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		wait= new WebDriverWait(driver,20);
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.manage().window().maximize();
	}

	@And("Load the URL")
	public void loadTheURL() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.get("http://leaftaps.com/opentaps");
	}
	
	@And("Wait till the Page Loads")
	public void timeOut()
	{
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}

	@And("Enter the Username")
	public void enterTheUsename() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@And("Enter the Password")
	public void enterThePassword() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("Click on Login Button")
	public void clickOnLoginButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("decorativeSubmit").click();
	
	
	}
	@Then("Verify User navigates to Home Page")
	public void verifyUserNavigatesToHomePage() {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("Navigated toHome screen");
	}

	@When("Click on CRMSFA Link")
	public void clickOnCRMSFALink() throws InterruptedException {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("Verify User navigates to Leads page")
	public void verifyUserNavigatesToLeadsPage() 
	{
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("User has been navigated to Leads Page");
	}
	
	@When("Click on Leads Link")
	public void clickOnLeadsLink() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	}
	
	@Then("Verify User navigates to My Leads page")
	public void verifyMyLeadsPage()
	{
		System.out.println("User has been navigated to My Leads Page");
	}
}
