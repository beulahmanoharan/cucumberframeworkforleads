package week6.day1.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteLeadCucumber extends LeadTest
{
	//Test
	String leadId="";

	//Login -> Step definitions are defined already in LoginCucumber class.
	//Step definitions for navigating to Find Leads screen are already defined in EditLeadCucumber class.
	@When("Click on Phone tab")
	public void clickOnPhoneTab() {
		driver.findElementByXPath("//span[text()='Phone']").click();
	}

	@Then("User navigates to the Page with Phone Number search criteria")
	public void userNavigatesToThePageWithPhoneNumberSearchCriteria()
	{
		System.out.println("User has been navigated to the Page with the search criteria Phone number");
	}

	@Given("Enter Phone Number as search criteria {string} {string} {string}")
	public void enterPhoneNumberAsSearchCriteriaCountryCodeAreaCodePhoneNumber(String countryCode, String areaCode, String phoneNum) {
		driver.findElementByName("phoneCountryCode").clear();
		driver.findElementByName("phoneCountryCode").sendKeys(countryCode);
		driver.findElementByName("phoneAreaCode").sendKeys(areaCode);
		driver.findElementByName("phoneNumber").sendKeys(phoneNum);
	}
	/*@When("Click on Find Leads button")
	public void click_on_Find_Leads_button() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}*/

/*	@Then("Search results are fetched")
	public void search_results_are_fetched() {
	    System.out.println("Search Results are fetched");
	}

	@Given("Wait until the grid loads")
	public void wait_until_the_grid_loads() {
		wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath("//div[text()='One Moment...']")));
	}
*/
	@When("Click on First Resulting Lead Id")
	public void click_on_First_Resulting_Lead()
	{
		WebElement firstLeadId=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[1]");
		leadId = firstLeadId.getText();
		System.out.println("First resulting lead Id in the grid..."+leadId);
		firstLeadId.click();
	}


	/*@Then("Verify User navigates to View Lead page")
	public void verify_User_navigates_to_View_Lead_page() {
	    System.out.println("User navigates to View Lead Page");
	}
*/
	@When("Click on Delete button")
	public void clickOnDeleteButton() {
		driver.findElementByLinkText("Delete").click();
	}

	//@Then("Verify User navigates to My Leads page") -> This step was defined already
	//@When("Click on Find Leads option")
	//@Then("Verify User navigates to Find Leads page")

	@Given("Enter Lead Id")
	public void enterLeadId() {
		driver.findElementByName("id").sendKeys(leadId);
	}

	//@When("Click on Find Leads button")
	//@Then("Search results are fetched")
	//@Given("Wait until the grid loads")

	@Then("Verify Error Message (.*)")
	public void verifyErrorMessage(String errorMessage)
	{
		String errorMsg = driver.findElementByClassName("x-paging-info").getText();
		System.out.println("Error Message is..."+errorMsg);

		if(errorMsg.equals(errorMessage))
			System.out.println("Error Message shown has been verified");
		else
			System.out.println("Error Message verification fails...");

	}

}
