package week6.day1.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadCucumber extends LeadTest {
	
	String fName="";
	String firstName="";

	//Login -> Step definitions are defined already in LoginCucumber class.
	@When("Click on Create Leads option")
	public void clickCreateLead()
	{
		driver.findElementByLinkText("Create Lead").click();
	}
	
	@Then("Verify User navigates to Create Lead page")
	public void verifyUserNavigatesToCreateLeadPage() {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("User has been navigated to Create Lead Page");
	}

	@Given("Enter First Name (.*)")
	public void enterFirstName(String fName) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		firstName=fName;
	}

	@Given("Enter Last Name (.*)")
	public void enterLastName(String lName) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@Given("Enter Company Name (.*)")
	public void enterCompanyName(String cName) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@When("Click on Create Leads button")
	public void clickOnCreateLeadsButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByName("submitButton").click();
	}

	@Then("New Lead has been Created")
	public void newLeadHasBeenCreated() {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("Lead has been created");
	}
	
	@Given("First Name is fetched")
	public void fetchFirstName()
	{
		fName = driver.findElementById("viewLead_firstName_sp").getText();
	}
	@Then("Verify First Name matches")
	public void VerifyFirstName()
	{
		if(fName.equals(firstName))
			System.out.println("First name is equal...");		
		else
			System.out.println("First name is not equal...");
		
	}
}
