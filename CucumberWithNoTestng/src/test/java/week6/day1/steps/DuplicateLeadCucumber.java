package week6.day1.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DuplicateLeadCucumber extends LeadTest
{
	String firstLeadNam="";
	String leadName ="";
	
	//Login -> Step definitions are defined already in LoginCucumber class.
	//Step definitions for navigating to Find Leads screen are already defined in EditLeadCucumber class.
	@When("Click on Email tab")
	public void clickOnEmailTab() {
		driver.findElementByLinkText("Email").click();
	}

	@Then("User navigates to the Page with EmailId search criteria")
	public void userNavigatesToThePageWithEmailIdSearchCriteria() {
	System.out.println("User navigated to the page with EmailId as search criteria");
	}

	@Given("Enter Email Id as search criteria (.*)")
	public void enterEmailIdAsSearchCriteria(String emailId) {
		driver.findElementByName("emailAddress").sendKeys(emailId);
	}
	
	/*@When("Click on Find Leads button")
	public void click_on_Find_Leads_button() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}

	@Then("Search results are fetched")
	public void search_results_are_fetched() {
	    System.out.println("Search Results are fetched");
	}

	@Given("Wait until the grid loads")
	public void wait_until_the_grid_loads() {
		wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath("//div[text()='One Moment...']")));
	}*/

	@When("Click on First Resulting Lead Name")
	public void click_on_First_Resulting_Lead() 
	{
		WebElement firstLeadName=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[3]");
		firstLeadNam = firstLeadName.getText();
		System.out.println("First resulting lead Name in the grid..."+firstLeadNam);
		firstLeadName.click();
	}
	
	
	/*@Then("Verify User navigates to View Lead page")
	public void verify_User_navigates_to_View_Lead_page() {
	    System.out.println("User navigates to View Lead Page");
	}*/

	@When("Click on Duplicate button")
	public void clickOnDuplicateButton() {
		driver.findElementByLinkText("Duplicate Lead").click();
	}

	@Then("Verify User navigates to Duplicate Lead page")
	public void duplicatePage()
	{
		System.out.println("User navigates to Duplicate Lead Page");
	}
	
	@When("Click Create Lead Button")
	public void clickCreateLead()
	{
		driver.findElementByName("submitButton").click();
	}
	
	//@Then("Verify User navigates to View Lead page")
	
	@Given("Duplicated Lead Name is fetched")
	public void firstNameIsFetched() {
		leadName = driver.findElementById("viewLead_firstName_sp").getText();
	}

	@Then("Verify Duplicated Lead Name matches")
	public void verifyFirstNameMatches() {
		if(firstLeadNam.equals(leadName))
			System.out.println("Captured Lead name is same as duplicated lead name...");
		else
			System.out.println("Captured lead name and duplicated lead name mismatches...");

	}

}
