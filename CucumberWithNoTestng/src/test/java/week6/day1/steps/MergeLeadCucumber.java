package week6.day1.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MergeLeadCucumber extends LeadTest
{
	Alert alert;
	String handleOfMainWindow="";
	String errorMsg="";

	@When("Click on Merge Leads option")
	public void click_on_Merge_Leads_option() {
		driver.findElementByLinkText("Merge Leads").click();
	}

	@Then("Verify User navigates to Merge Leads page")
	public void verify_User_navigates_to_Merge_Leads_page() {
	    System.out.println("User navigates to Merge Leads Page");
	}

	@When("Click on From Lead Icon")
	public void click_on_From_Lead_Icon() {
		driver.findElementByXPath("//input[@id='partyIdFrom']/following::img").click();
	}

	@When("Switch to Find Leads page")
	public void switch_to_Find_Leads_page() 
	{
		Set<String> winHandles = driver.getWindowHandles();
		List<String> windowHandles = new ArrayList<String>();
		windowHandles.addAll(winHandles);
		driver.switchTo().window(windowHandles.get(1));
		handleOfMainWindow=windowHandles.get(0);
	}
	
	@Given("Enter Lead Id (.*)")
	public void enterLead(String data)
	{
		driver.findElementByName("id").sendKeys(data);
	}

	@When("Click on Resulting Lead Id")
	public void click_on_Resulting_Lead_Id() {
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//a").click();
	}
	
	@Then("Switch to Merge Leads page")
	public void switch_to_Merge_Leads_page() {
		driver.switchTo().window(handleOfMainWindow);
	}

	@When("Click on To Lead Icon")
	public void click_on_To_Lead_Icon() {
		driver.findElementByXPath("//input[@id='partyIdTo']/following::img").click();
	}
	
	@When("Switch to Find Leads page again")
	public void switch_to_Find_Leads_page_again() 
	{
		Set<String> winHandles = driver.getWindowHandles();
		List<String> windowHandles = new ArrayList<String>();
		windowHandles.addAll(winHandles);
		driver.switchTo().window(windowHandles.get(1));
		handleOfMainWindow=windowHandles.get(0);
	}
	
	@When("User clicks on Merge button")
	public void user_clicks_on_Merge_button() {
		driver.findElementByLinkText("Merge").click();
	}

	@When("Switch to Alert")
	public void switch_to_Alert() {
		alert = driver.switchTo().alert();
	}

	@When("Accept the Alert")
	public void accept_the_Alert() {
		alert.accept();
	}

	//@Then("Verify Error Message (.*)") -> Step defined already
	

}
