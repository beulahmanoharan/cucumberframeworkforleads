package week6.day1.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EditLeadCucumber extends LeadTest
{
	String leadId="";
	String company="";
	String newCompanyName ="";

	@When("Click on Find Leads option")
	public void click_on_Find_Leads_option() 
	{
		driver.findElementByLinkText("Find Leads").click();
	}

	@Then("Verify User navigates to Find Leads page")
	public void verify_User_navigates_to_Find_Leads_page() 
	{
		System.out.println("User navigates to Find Leads Page");
	}

	@Given("Enter the First Name as search criteria (.*)")
	public void enter_First_Name_as_search_criteria(String firstName) 
	{
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(firstName);
	}

	@When("Click on Find Leads button")
	public void click_on_Find_Leads_button() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}

	@Then("Search results are fetched")
	public void search_results_are_fetched() {
	    System.out.println("Search Results are fetched");
	}

	@Given("Wait until the grid loads")
	public void wait_until_the_grid_loads() {
		wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath("//div[text()='One Moment...']")));
	}

	@When("Click on First Resulting Lead")
	public void click_on_First_Resulting_Lead() 
	{
		WebElement firstLeadId=driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[1]");
		leadId = firstLeadId.getText();
		System.out.println("First resulting lead Id in the grid..."+leadId);
		firstLeadId.click();
	}

	@Then("Verify User navigates to View Lead page")
	public void verify_User_navigates_to_View_Lead_page() {
	    System.out.println("User navigates to View Lead Page");
	}

	@When("Click on Edit button")
	public void click_on_Edit_button() {
		driver.findElementByLinkText("Edit").click();
	}

	@Then("Verify Fields are editable")
	public void verify_Fields_are_editable() {
	    System.out.println("Fields are editable");
	}

	@Given("Update Company Name (.*)")
	public void update_Company_Name(String companyName) {
		WebElement companyNam = driver.findElementById("updateLeadForm_companyName");
		companyNam.clear();
		companyNam.sendKeys(companyName);
		company=companyName;
	}

	@When("Click Update Lead button")
	public void click_Update_Lead_button() {
		driver.findElementByName("submitButton").click();
	}

	@Then("Verify user navigates to View Lead page")
	public void verify_user_navigates_to_View_Lead_page() {
	    System.out.println("User navigates to View Lead Page");
	}
	
	@Given("Updated Company Name is fetched")
	public void fetchUpdatedCompanyName()
	{
		newCompanyName = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println("Updated company name along with Lead Id.."+newCompanyName);
	}
	
	@Then("Verify the updated company name matches")
	public void verifyCompanyName()
	{
		if(newCompanyName.contains(company))
			System.out.println("Update Company Name is verified");
		else
			System.out.println("Update Company Name mismatches");
	}
}
