package week6.day1.steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class LearnHooks extends LeadTest {
	
	@Before
	public void beforeScenario(Scenario s)
	{
		System.out.println("Id of the executing method..."+s.getId());
		System.out.println("Method executing..."+s.getName());
	}
	@After
	public void afterScenario(Scenario s)
	{
		System.out.println("Method executing..."+s.getStatus());
		driver.close();
	}

}
